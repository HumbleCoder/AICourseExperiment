﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TEST5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Random R = new Random();
        int Bingo;

        int times=0, bingotimes = 0;

        private void Button1_Click(object sender, EventArgs e)
        {
            Bingo=R.Next(0,100);
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                times++;
                int a = Convert.ToInt32(textBox1.Text);
                if (a > Bingo)
                {
                    if (a - Bingo > 10)
                    {
                        MessageBox.Show("太大了");
                    }
                    if (a - Bingo <= 10)
                    {
                        MessageBox.Show("大了一点");
                    }

                }
                if (a < Bingo)
                {
                    if (Bingo-a > 10)
                    {
                        MessageBox.Show("太小了");
                    }
                    if (Bingo-a <= 10)
                    {
                        MessageBox.Show("小了一点");
                    }

                }
                if (a == Bingo)
                {
                    bingotimes++;
                    button1.PerformClick();
                    MessageBox.Show("恭喜你猜对了");
                }

                label3.Text = times.ToString() + "次";
                label5.Text = bingotimes.ToString() + "次";

                float f = (float)bingotimes / (float)times * 100;
                int res = Convert.ToInt32(f);

                label7.Text = res.ToString() + "%";
            }
            catch
            {
                    MessageBox.Show("输入有误，请输入整数");
            }
        }
    }
}
