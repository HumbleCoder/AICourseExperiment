﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TEST6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double nowdeg = 0.0,tardeg = 0.0, tarspd = 0.0;

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if(nowdeg<tardeg)
            {
                nowdeg = nowdeg + tarspd * ((double)timer1.Interval / 1000.0);
            }
            arrow(nowdeg);
            GC.Collect();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox1.Refresh();

            nowdeg = 0;
            timer1.Stop();
            timer1.Enabled = false;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                tardeg = Convert.ToDouble(textBox1.Text);
                tarspd = Convert.ToDouble(textBox2.Text);
                timer1.Enabled = true;
                timer1.Start();
            }
            catch
            {
                MessageBox.Show("输入有误，请输入正确的角度和角速度");
            }

            
        }

        private void arrow(double ang)
        {
            //创建画布
            Bitmap canvas = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics g = Graphics.FromImage(canvas);

            //创建画笔
            Pen redPen = new Pen(Color.Red, 10);

            //将画笔定制为带箭头画笔
            redPen.EndCap = LineCap.ArrowAnchor;

            //画线
            //角度转换成弧度
            ang = (ang * Math.PI / 180);
                                //起点x                  起点y                        终点x                                                                                    终点y
            g.DrawLine(redPen, pictureBox1.Width/2, pictureBox1.Height/2, (float)(pictureBox1.Width / 2 * Math.Sin(ang) + pictureBox1.Width / 2), (float)(pictureBox1.Height / 2 - pictureBox1.Height / 2 * Math.Cos(ang)));

            //释放资源
            redPen.Dispose();
            g.Dispose();

            //绘制结果显示
            pictureBox1.Image = canvas;

        }
    }
}
