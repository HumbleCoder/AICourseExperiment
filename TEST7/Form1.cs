﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TEST7
{
    public partial class Form1 : Form
    {
        //定义结构体
        struct DATA
        {
            public int[,] OriData;//原始数据数组
            public int[,] SortData;//排序后数据数组
            public int SUM;//总和
            public double AVE;//全平均
        }
        DATA Data1;//创建结构体对象


        //文件目录及文件名
        string filePath = @"c:\LOG\";

        string fileName1 = @"c:\LOG\Oridata.csv";

        string fileName2 = @"c:\LOG\Sortdata.csv";

        //设定文件内码 gb2312 简体中文
        Encoding enc = Encoding.GetEncoding("gb2312");

        public Form1()
        {
            InitializeComponent();
        }

        //特殊二元数组用冒泡算法
        public void bubbleSortSTL(int[,] arr, int n, int m)
        {//                       二元数组    行数    列数 
            int t;
            for(int k=0;k<m;k++)
            {
                for (int i = 0; i < n - 1; i++)
                    for (int j = 0; j < n - i - 1; j++)
                        if (arr[j + 1, k] < arr[j, k])
                        {
                            t = arr[j + 1, k];
                            arr[j + 1, k] = arr[j, k];
                            arr[j, k] = t;
                        }
            }
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //为结构体内部数据初始化
            Data1.OriData = new int[500, 6];
            Data1.SortData = new int[500, 6];
            Data1.AVE = 0.0;
            Data1.SUM = 0;

            //进度条
            progressBar1.Maximum = 500;
            progressBar1.Value = 0;
            label11.Text = "原始随机数据生成中...";
            this.Refresh();

            //产生随机原始数据
            for (int i = 0; i < 500; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    //使用全局位移标识符GUID的哈希值为种子产生随机数（随机性较高）
                    var r = new Random(Guid.NewGuid().GetHashCode());
                    Data1.OriData[i, j] = r.Next(0, 1000);//存储原始数据
                    Data1.SUM += Data1.OriData[i, j];//计算全数据总和
                }
                //计算每一行的总和
                Data1.OriData[i, 5] = Data1.OriData[i, 0] + Data1.OriData[i, 1] + Data1.OriData[i, 2] + Data1.OriData[i, 3] + Data1.OriData[i, 4];

                progressBar1.Value++;
            }
            //计算全平均
            Data1.AVE = (double)Data1.SUM / 2500.0;


            label11.Text = "排序数据生成中...";
            this.Refresh();

            //将数据复制给排序后的数组，对排序后数组初始化
            Data1.SortData = (int[,])Data1.OriData.Clone();
            //用冒泡算法对每一列进行排序
            bubbleSortSTL(Data1.SortData, 500, 5);

            //对排序后的数组进行每行总和计算
            for (int i = 0; i < 500; i++)
            {
                Data1.SortData[i, 5] = Data1.SortData[i, 0] + Data1.SortData[i, 1] + Data1.SortData[i, 2] + Data1.SortData[i, 3] + Data1.SortData[i, 4];
            }

            progressBar1.Maximum = 500;
            progressBar1.Value = 0;
            label11.Text = "原始数据显示中...";
            this.Refresh();

            //显示原始数据
            for (int i = 0; i < 500; i++)
            {
                textBox1.Text += Data1.OriData[i, 0];
                textBox1.Text += Environment.NewLine;

                textBox2.Text += Data1.OriData[i, 1];
                textBox2.Text += Environment.NewLine;

                textBox3.Text += Data1.OriData[i, 2];
                textBox3.Text += Environment.NewLine;

                textBox4.Text += Data1.OriData[i, 3];
                textBox4.Text += Environment.NewLine;

                textBox5.Text += Data1.OriData[i, 4];
                textBox5.Text += Environment.NewLine;

                textBox6.Text += Data1.OriData[i, 5];
                textBox6.Text += Environment.NewLine;

                progressBar1.Value++;
            }

            //显示总和，全平均
            label8.Text = Data1.SUM.ToString();
            label9.Text = Data1.AVE.ToString();

            label11.Text = "原始数据显示完成";
            this.Refresh();

            progressBar1.Maximum = 500;
            progressBar1.Value = 0;
            label11.Text = "排序后数据显示中...";
            this.Refresh();

            //显示排序后数据
            for (int i = 0; i < 500; i++)
            {
                textBox12.Text += Data1.SortData[i, 0];
                textBox12.Text += Environment.NewLine;

                textBox11.Text += Data1.SortData[i, 1];
                textBox11.Text += Environment.NewLine;

                textBox9.Text += Data1.SortData[i, 2];
                textBox9.Text += Environment.NewLine;

                textBox10.Text += Data1.SortData[i, 3];
                textBox10.Text += Environment.NewLine;

                textBox7.Text += Data1.SortData[i, 4];
                textBox7.Text += Environment.NewLine;

                textBox8.Text += Data1.SortData[i, 5];
                textBox8.Text += Environment.NewLine;

                progressBar1.Value++;
            }

            //显示总和，全平均
            label14.Text = Data1.SUM.ToString();
            label12.Text = Data1.AVE.ToString();

            label11.Text = "排序后数据显示完成";
            this.Refresh();

            progressBar1.Maximum = 1000;
            progressBar1.Value = 0;
            label11.Text = "文件写入中...";
            this.Refresh();

            //准备写入文件，检查文件夹是否存在，若不存在则新建
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            //文件标题
            String title1 = "原始1列,原始2列,原始3列,原始4列,原始5列,行总和\n";
            File.AppendAllText(fileName1, title1, enc);//写入

            //将原始数据写入文件
            for (int i = 0; i < 500; i++)
            {
                string dataforfile = Data1.OriData[i, 0].ToString() + ',' + Data1.OriData[i, 1].ToString() + ',' + Data1.OriData[i, 2].ToString() + ',' + Data1.OriData[i, 3].ToString() + ',' + Data1.OriData[i, 4].ToString() + ',' + Data1.OriData[i, 5].ToString() + '\n';
                File.AppendAllText(fileName1, dataforfile, enc);
                progressBar1.Value++;
            }

            //文件标题
            String title2 = "排序1列,排序2列,排序3列,排序4列,排序5列,行总和\n";
            File.AppendAllText(fileName2, title2, enc);

            //将排序后数据写入文件
            for (int i = 0; i < 500; i++)
            {
                string dataforfile = Data1.SortData[i, 0].ToString() + ',' + Data1.SortData[i, 1].ToString() + ',' + Data1.SortData[i, 2].ToString() + ',' + Data1.SortData[i, 3].ToString() + ',' + Data1.SortData[i, 4].ToString() + ',' + Data1.SortData[i, 5].ToString() + '\n';
                File.AppendAllText(fileName2, dataforfile, enc);
                progressBar1.Value++;
            }

            label11.Text = "文件写入完成";
            this.Refresh();

            MessageBox.Show("全部数据显示，文件输出完成");




        }
    }
}
