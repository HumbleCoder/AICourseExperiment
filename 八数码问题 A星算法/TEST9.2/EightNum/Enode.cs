﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EightNum
{
    public class Enode
    {
        public byte[,] key;//该节点存储的矩阵
        public bool istoUp, istoDown, istoLeft, istoRight;//表示是否可以向该方向推
        
        public int parent;//父节点的序号
        public int storey;//所处的深度，从1开始

        public Enode(int dir, int parent, byte[,] key, int storey)//dir方向：1上 2下 3左 4右  parent：父节点编号   key：3*3矩阵  deepth:所属层数 
        {
            this.parent = parent;
            this.key = key;
            this.storey = storey;

            this.istoDown = true;
            this.istoUp = true;
            this.istoRight = true;
            this.istoLeft = true;

            switch (dir)
            {
                case 1:
                    {
                        this.istoDown = false;
                        break;
                    }
                case 2:
                    {
                        this.istoUp = false;
                        break;
                    }
                case 3:
                    {
                        this.istoRight = false;
                        break;
                    }
                case 4:
                    {
                        this.istoLeft = false;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (key[i, j] == 0)
                    {
                        //判断方向的可推性
                        if (i == 0)
                        {
                            this.istoUp = false;
                        }

                        if (i == 2)
                        {
                            this.istoDown = false;
                        }

                        if (j == 0)
                        {
                            this.istoLeft = false;
                        }

                        if (j == 2)
                        {
                            this.istoRight = false;
                        }
                    }
                }
            }

        }
    }
}
