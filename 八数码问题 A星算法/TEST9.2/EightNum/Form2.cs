﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EightNum
{
    public partial class Form2 : Form
    {
        String[] t = new String[9];

        public Form2()
        {
            InitializeComponent();
        }

        public String[] getT(){
            return t;
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            t[0] = this.textBox1.Text;
            t[1] = this.textBox2.Text;
            t[2] = this.textBox3.Text;
            t[3] = this.textBox4.Text;
            t[4] = this.textBox5.Text;
            t[5] = this.textBox6.Text;
            t[6] = this.textBox7.Text;
            t[7] = this.textBox8.Text;
            t[8] = this.textBox9.Text;

            int m = 0;
            for (int i = 0; i < 9; i++)
            {
                if (t[i] == ""||t[i] == " "||t[i] == "  ")
                    m = m + 9;
                else if (t[i] == "1")
                    m = m + 1;
                else if (t[i] == "2")
                    m = m + 2;
                else if (t[i] == "3")
                    m = m + 3;
                else if (t[i] == "4")
                    m = m + 4;
                else if (t[i] == "5")
                    m = m + 5;
                else if (t[i] == "6")
                    m = m + 6;
                else if (t[i] == "7")
                    m = m + 7;
                else if (t[i] == "8")
                    m = m + 8;
            }
            if (m == 45)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("输入的目标状态不符合规范", "提醒", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
