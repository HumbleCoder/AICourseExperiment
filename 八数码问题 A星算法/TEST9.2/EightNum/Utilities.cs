﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EightNum
{
    static class Utilities
    {
        public static bool checkUp(byte[,] a, byte[,] b)
        {
            bool re = true;
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (a[i, j] != b[i, j])
                        re = false;
                }
            }
            return re;
        }
        //判断是否有解：
        //一个状态表示成一维的形式，求出除0之外所有数字的逆序数之和，
        //也就是每个数字前面比它大的数字的个数的和，称为这个状态的逆序。
        //若两个状态的逆序奇偶性 相同，则可相互到达，否则不可相互到达。

        public static bool checkHaveAnswer(byte[,] a, byte[,] b)
        {
            int x, sa = 0, sb = 0, i, j;

            for (x = 1; x < 9; x++)
                for (i = 0; i < 3; i++)
                    for (j = 0; i * 3 + j < x && j < 3; j++)
                    {
                        if (a[i, j] != 0 && a[i, j] < a[x / 3, x % 3])
                            sa++;
                    }
            for (x = 1; x < 9; x++)
                for (i = 0; i < 3; i++)
                    for (j = 0; i * 3 + j < x && j < 3; j++)
                    {
                        if (b[i, j] != 0 && b[i, j] < b[x / 3, x % 3])
                            sb++;
                    }

            return sa % 2 == sb % 2;
        }

        public static void switchKey(ref byte a, ref byte b)//交换元素（注意引用）
        {
            byte temp;
            temp = a;
            a = b;
            b = temp;
        }

        public static void makeEqule(byte[,] a, byte[,] b)//b的值赋给a数组的引用
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    a[i, j] = b[i, j];
                }
            }
        }

        public static String stateStr(byte[,] t)
        {
            String str = "********\r\n";
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    str = str + t[i, j].ToString() + " ";
                str = str + "\r\n";
            }
            return str + "\r\n";
        }

        public static byte[,] getMoveRight(byte[,] t)//获取移动矩阵（非引用）
        {
            byte[,] a = new byte[3, 3];
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    a[i, j] = t[i, j];
                }
            }

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (a[i, j] == 0)
                    {
                        Utilities.switchKey(ref a[i, j], ref a[i, j + 1]);
                        return a;
                    }
                }
            }
            return a;
        }

        public static byte[,] getMoveUp(byte[,] t)
        {
            byte[,] a = new byte[3, 3];
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    a[i, j] = t[i, j];
                }
            }

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (a[i, j] == 0)
                    {
                        Utilities.switchKey(ref a[i, j], ref  a[i - 1, j]);
                        return a;

                    }
                }
            }

            return a;
        }

        public static byte[,] getMoveLeft(byte[,] t)
        {
            byte[,] a = new byte[3, 3];
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    a[i, j] = t[i, j];
                }
            }

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (a[i, j] == 0)
                    {

                        Utilities.switchKey(ref a[i, j], ref a[i, j - 1]);
                        return a;

                    }
                }
            }

            return a;
        }

        public static byte[,] getMoveDown(byte[,] t)
        {
            byte[,] a = new byte[3, 3];
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    a[i, j] = t[i, j];
                }
            }

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (a[i, j] == 0)
                    {
                        Utilities.switchKey(ref a[i, j], ref  a[i + 1, j]);
                        return a;//一定要返回了！！！！！！！！！！！！！！！！！！！

                    }
                }
            }

            return a;
        }

        public static bool moveRight(byte[,] t)//矩阵引用移动
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (t[i, j] == 0)
                    {
                        if (j == 2)
                            return false;
                        else
                        {
                            Utilities.switchKey(ref t[i, j], ref t[i, j + 1]);
                            return true;
                        }

                    }
                }
            }
            return false;
        }

        public static bool moveUp(byte[,] t)
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (t[i, j] == 0)
                    {
                        if (i == 0)
                            return false;
                        else
                        {
                            Utilities.switchKey(ref t[i, j], ref  t[i - 1, j]);
                            return true;
                        }

                    }
                }
            }
            return false;
        }

        public static bool moveLeft(byte[,] t)
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (t[i, j] == 0)
                    {
                        if (j == 0)
                            return false;
                        else
                        {
                            Utilities.switchKey(ref t[i, j], ref t[i, j - 1]);
                            return true;
                        }

                    }
                }
            }
            return false;
        }

        public static bool moveDown(byte[,] t)
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if (t[i, j] == 0)
                    {
                        if (i == 2)
                            return false;
                        else
                        {
                            Utilities.switchKey(ref t[i, j], ref t[i + 1, j]);
                            return true;
                        }

                    }
                }
            }
            return false;
        }
    }
}
