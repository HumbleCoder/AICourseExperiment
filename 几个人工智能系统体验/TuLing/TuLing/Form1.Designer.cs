﻿namespace TuLing
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_send = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.rtb_mess = new System.Windows.Forms.RichTextBox();
            this.rtb_send = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(269, 374);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(50, 30);
            this.btn_send.TabIndex = 0;
            this.btn_send.Text = "发送";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(366, 374);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(50, 30);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "关闭";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // rtb_mess
            // 
            this.rtb_mess.Location = new System.Drawing.Point(12, 12);
            this.rtb_mess.Name = "rtb_mess";
            this.rtb_mess.Size = new System.Drawing.Size(454, 248);
            this.rtb_mess.TabIndex = 2;
            this.rtb_mess.Text = "";
            // 
            // rtb_send
            // 
            this.rtb_send.Location = new System.Drawing.Point(12, 279);
            this.rtb_send.Name = "rtb_send";
            this.rtb_send.Size = new System.Drawing.Size(454, 79);
            this.rtb_send.TabIndex = 3;
            this.rtb_send.Text = "";
            this.rtb_send.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtb_send_KeyDown);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 416);
            this.Controls.Add(this.rtb_send);
            this.Controls.Add(this.rtb_mess);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_send);
            this.Name = "MainForm";
            this.Text = "图灵对话";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.RichTextBox rtb_mess;
        private System.Windows.Forms.RichTextBox rtb_send;
    }
}

