using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Ant
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DoInitial();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(PlayGround.MaxX + 10, PlayGround.MaxY + 10); // 新建画布
            Graphics g = Graphics.FromImage(bmp);  // 获取这块内存画布的Graphics引用
            Pen pb = new Pen(_borderColor, 2); // 画边框
            Pen ph = new Pen(_homeColor, 5); // 画窝
            Pen pf = new Pen(_foodColor, 5); // 画食物
            g.DrawEllipse(pf, PlayGround.FoodLocation.X, PlayGround.FoodLocation.Y, 6, 6);
            g.DrawEllipse(ph, PlayGround.HomeLocation.X, PlayGround.HomeLocation.Y, 6, 6);
            g.DrawRectangle(pb, 1, 1, PlayGround.MaxX + 2, PlayGround.MaxY + 2);
            if (StoneInitialFlag)
            {
                StoneInitialFlag = false;
                PlayGround.InitialStone();
            }
            DrawStone(bmp); // 画障碍物
            this.CreateGraphics().DrawImage(bmp, 0, 0); // 显示新建的画布
            bmp.Dispose();      // 释放内存
        }

        private void DoInitial()
        {
            InitialAnt();
            textBox1.Text = timer1.Interval.ToString();
            textBox2.Text = Antc._smellDropRate.ToString();
            textBox3.Text = Antc._errorRate.ToString();
            textBox4.Text = Antc._eyeShot.ToString();
            textBox5.Text = Antc._maxSmell.ToString();
            textBox6.Text = Antc._maxTrace.ToString();
            textBox7.Text = PlayGround._smellGoneRate.ToString();
            textBox8.Text = PlayGround._maxStone.ToString();
            textBox9.Text = _maxAnt.ToString();
            //InitialFood();
        }

        private void InitialAnt()
        {
            Ants = new Antc[MaxAnt];
            Random ran = new Random();
            List<int> _ranList = new List<int>();
            int rannum;
            for (int i = 0; i < MaxAnt; i++)
            {
                do
                {
                    rannum = ran.Next(10000);
                }
                while (_ranList.Contains(rannum));
                _ranList.Add(rannum);
                Ants[i] = new Antc(rannum);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        /* 1、调用蚂蚁移动的函数
           2、画蚂蚁的移动
         * 3、画窝和食物
        */
        {
            try
            {
                Graphics g = CreateGraphics();
                Pen pc = new Pen(this.BackColor, 2); // 清除蚂蚁
                Pen pa = new Pen(_antColor, 2); // 画蚂蚁
                Pen paf = new Pen(_antfColor, 2); // 画带着食物的蚂蚁
                for (int i = 0; i < MaxAnt; i++)
                {
                    if (!checkBox3.Checked)
                    {
                        g.DrawEllipse(pc, Ants[i].GetX(), Ants[i].GetY(), Antc.AntSize, Antc.AntSize);
                    }
                    else if (ClearAntCounter<MaxAnt*2+10)
                    {
                        ClearAntCounter++;
                        g.DrawEllipse(pc, Ants[i].GetX(), Ants[i].GetY(), Antc.AntSize, Antc.AntSize);
                    }
                    Ants[i].AntOneStep();
                    if (!checkBox3.Checked)
                    {
                        if (Ants[i].HaveFood()) g.DrawEllipse(paf, Ants[i].GetX(), Ants[i].GetY(), Antc.AntSize, Antc.AntSize);
                        else g.DrawEllipse(pa, Ants[i].GetX(), Ants[i].GetY(), Antc.AntSize, Antc.AntSize);
                    }
                }
                Pen ph = new Pen(_homeColor, 5); // 画窝
                Pen pf = new Pen(_foodColor, 5); // 画食物
                g.DrawEllipse(pf, PlayGround.FoodLocation.X, PlayGround.FoodLocation.Y, 6, 6);
                g.DrawEllipse(ph, PlayGround.HomeLocation.X, PlayGround.HomeLocation.Y, 6, 6);
            }
            catch (Exception ee)
            {
                //label2.Text = ee.Message; 状态显示
            }
        }

        Thread Timer2Trd;
        private void timer2_Tick(object sender, EventArgs e)
        /* 处理一些速度较慢的操作
         * 1、调用场地处理的函数，信息素的变化
         * 2、画信息素
         * 3、画蚂蚁的路径 ？
         */
        {
            Timer2Trd = new Thread(new ThreadStart(this.DoTimer2));
            Timer2Trd.Start();
        }

        private void DoTimer2()
        {
            try
            {
                // 显示信息
                ShowMessageCounter++;
                if (ShowMessageCounter >= 2)
                {
                    ShowMessageCounter = 0;
                    // 优先显示ShowMessage，而且只显示一次
                    if (!string.IsNullOrEmpty(PlayGround.ShowMessage))
                    {
                        label2.Text = PlayGround.ShowMessage;
                        PlayGround.ShowMessage = "";
                    }
                    // 如果ShowMessage显示过，则显示ShowMessage2
                    else if (!string.IsNullOrEmpty(PlayGround.ShowMessage2))
                    {
                        label2.Text = PlayGround.ShowMessage2;
                    }
                }
                
                // 使用双缓冲
                // Graphics g = CreateGraphics();         // 这个是原来的，会闪烁
                // 1、在内存中建立一块“虚拟画布”： 
                Bitmap bmp = new Bitmap(PlayGround.MaxX + 10, PlayGround.MaxY + 10);
                // 2、获取这块内存画布的Graphics引用：
                // Graphics g = Graphics.FromImage(bmp);
                // 3、在这块内存画布上绘图：
                // g.FillEllipse(brush, i * 10, j * 10, 10, 10);

                // 调用函数处理信息素的变化
                DealSmellCounter++;
                if (DealSmellCounter >= 5)
                {
                    DealSmellCounter = 0;
                    // 先清除上次画的信息素
                    if (checkBox1.Checked||checkBox2.Checked)
                    {
                        ClearSmell(bmp);
                    }
                    else if (ClearSmellCounter < 2)
                    {
                        ClearSmellCounter++;
                        ClearSmell(bmp);
                    }
                    PlayGround.DealGround();
                    DrawSmell(bmp);
                    DrawSmellCounter = -1;
                }
                // 画信息素
                if (checkBox1.Checked || checkBox2.Checked)
                {
                    DrawSmellCounter++;
                    if (DrawSmellCounter >= 1)
                    {
                        DrawSmellCounter = 0;
                        DrawSmell(bmp);
                    }
                }
                // 4、将内存画布画到窗口中
                this.CreateGraphics().DrawImage(bmp, 0, 0);
                // 5、释放内存
                bmp.Dispose();
            }
            catch (Exception ee)
            {
               // label2.Text = ee.Message;
            }
        }

        private void DrawSmell(Graphics g) // 现在不用了，用bmp直接画点
        {
            //Pen phsm = new Pen(_hSmellColor, 1);
            //Pen pfsm = new Pen(_fSmellColor, 1);
            int hsm, fsm;
            for (int xxx = 3; xxx < PlayGround.MaxX; xxx++)
                for (int yyy = 3; yyy < PlayGround.MaxY; yyy++)
                {
                    hsm = PlayGround.HomeSmell[xxx, yyy];
                    fsm = PlayGround.FoodSmell[xxx, yyy];
                    if (hsm <= 0 && fsm <= 0) continue;
                    if (checkBox1.Checked && hsm > 0)
                    {
                        int co = hsm > 200 ? 0 : 200 - hsm;
                        g.DrawRectangle(new Pen(Color.FromArgb(co, 255, co)), xxx, yyy, 1, 1);
                    }
                    if (checkBox2.Checked && fsm > 0)
                    {
                        int co = fsm > 200 ? 0 : 200 - fsm;
                        g.DrawRectangle(new Pen(Color.FromArgb(co, co, 255)), xxx, yyy, 1, 1);
                    }
                    //if (hsm > 0 && checkBox1.Checked) g.DrawRectangle(phsm, xxx, yyy, 1, 1);
                    //if (fsm > 0 && checkBox2.Checked) g.DrawRectangle(pfsm, xxx, yyy, 1, 1);
                }
        }

        private void DrawSmell(Bitmap bmp)
        {
            int hsm, fsm;
            for (int xxx = 3; xxx < PlayGround.MaxX; xxx++)
                for (int yyy = 3; yyy < PlayGround.MaxY; yyy++)
                {
                    hsm = PlayGround.HomeSmell[xxx, yyy];
                    fsm = PlayGround.FoodSmell[xxx, yyy];
                    if (hsm <= 0 && fsm <= 0) continue;
                    if (checkBox1.Checked && hsm > 0)
                    {
                        int co = hsm > 200 ? 0 : 200 - hsm;
                        bmp.SetPixel(xxx, yyy, Color.FromArgb(co, 255, co));
                        //g.DrawRectangle(new Pen(Color.FromArgb(co, 255, co)), xxx, yyy, 1, 1);
                    }
                    if (checkBox2.Checked && fsm > 0)
                    {
                        int co = fsm > 200 ? 0 : 200 - fsm;
                        bmp.SetPixel(xxx, yyy, Color.FromArgb(co, co, 255));
                        //g.DrawRectangle(new Pen(Color.FromArgb(co, co, 255)), xxx, yyy, 1, 1);
                    }
                }
        }

        private void DrawStone(Graphics g) // 现在不用了，用bmp直接画点
        {
            Pen ps = new Pen(_stoneColor, 1); // 画障碍物
            for (int xxx = 3; xxx < PlayGround.MaxX; xxx++)
                for (int yyy = 3; yyy < PlayGround.MaxY; yyy++)
                {
                    if (PlayGround.IsStone(xxx, yyy)) g.DrawRectangle(ps, xxx, yyy, 1, 1);
                }
        }

        private void DrawStone(Bitmap bmp)
        {
            for (int xxx = 3; xxx < PlayGround.MaxX; xxx++)
                for (int yyy = 3; yyy < PlayGround.MaxY; yyy++)
                {
                    if (PlayGround.IsStone(xxx, yyy)) bmp.SetPixel(xxx, yyy, _stoneColor);
                }
        }

        private void ClearSmell(Graphics g) // 现在不用了，用bmp直接画点
        {
            Pen pcsm = new Pen(this.BackColor, 1);
            for (int xxx = 3; xxx < PlayGround.MaxX; xxx++)
                for (int yyy = 3; yyy < PlayGround.MaxY; yyy++)
                {
                    if (PlayGround.HomeSmell[xxx, yyy] <= 0 && PlayGround.FoodSmell[xxx, yyy] <= 0) continue;
                    g.DrawRectangle(pcsm, xxx, yyy, 1, 1);
                }
        }

        private void ClearSmell(Bitmap bmp)
        {
            for (int xxx = 3; xxx < PlayGround.MaxX; xxx++)
                for (int yyy = 3; yyy < PlayGround.MaxY; yyy++)
                {
                    if (PlayGround.HomeSmell[xxx, yyy] <= 0 && PlayGround.FoodSmell[xxx, yyy] <= 0) continue;
                    bmp.SetPixel(xxx, yyy, this.BackColor);
                }
        }

// ==================  界面处理子函数  ==============================

        private void button1_Click(object sender, EventArgs e) // 开始-暂停
        {
            if (!timer1.Enabled)
            {
                timer1.Enabled = true;
                timer2.Enabled = true;
                button1.Text = "暂停";
            }
            else
            {
                timer1.Enabled = false;
                timer2.Enabled = false;
                button1.Text = "开始";
            }
        }

        private void button7_Click(object sender, EventArgs e) // 重新开始
        {
            DoRestart();
        }

        private void DoRestart()
        {
            bool t1State, t2State;
            t1State = timer1.Enabled;
            t2State = timer2.Enabled;
            timer1.Enabled = false;
            timer2.Enabled = false;
            PlayGround.ShowMessage = "";
            PlayGround.ShowMessage2 = "";
            Bitmap bmp = new Bitmap(PlayGround.MaxX + 10, PlayGround.MaxY + 10); // 新建画布
            Graphics g = Graphics.FromImage(bmp);  // 获取这块内存画布的Graphics引用
            g.Clear(this.BackColor);
            PlayGround.InitialPlayGround();
            if (checkBox4.Checked) PlayGround.InitialStone();
            InitialAnt();
            Pen pb = new Pen(_borderColor, 2); // 画边框
            Pen ph = new Pen(_homeColor, 2); // 画窝
            Pen pf = new Pen(_foodColor, 2); // 画食物
            g.DrawEllipse(pf, PlayGround.FoodLocation.X, PlayGround.FoodLocation.Y, 6, 6);
            g.DrawEllipse(ph, PlayGround.HomeLocation.X, PlayGround.HomeLocation.Y, 6, 6);
            g.DrawRectangle(pb, 1, 1, PlayGround.MaxX + 3, PlayGround.MaxY + 3);
            DrawStone(bmp);
            this.CreateGraphics().DrawImage(bmp, 0, 0); // 显示新建的画布
            bmp.Dispose();      // 释放内存
            timer1.Enabled = t1State;
            timer2.Enabled = t2State;
        }

        private void button11_Click(object sender, EventArgs e) // 保存地图
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e) // 保存文件
        {
            string filename = saveFileDialog1.FileName;
            DoSaveMap(filename);
        }

        private void DoSaveMap(string filename)
        {
            try
            {
                Bitmap bmp = new Bitmap(PlayGround.MaxX, PlayGround.MaxY);
                DrawStone(bmp);
                bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Bmp);
                bmp.Dispose();
            }
            catch (Exception ee)
            {
                PlayGround.ShowMessage = ee.Message;
                label2.Text = PlayGround.ShowMessage;
            }
            string sfn = filename.IndexOf("\\") != -1 ? filename.Substring(filename.LastIndexOf("\\") + 1) : filename;
            PlayGround.ShowMessage = string.Format("地图已保存 - {0}", sfn);
            label2.Text = PlayGround.ShowMessage;
        }

        private void button12_Click(object sender, EventArgs e) // 调入地图
        {
            bool t1State, t2State;
            t1State = timer1.Enabled;
            t2State = timer2.Enabled;
            timer1.Enabled = false;
            timer2.Enabled = false;
            PlayGround.ShowMessage = "";
            PlayGround.ShowMessage2 = "";
            openFileDialog1.ShowDialog();
            timer1.Enabled = t1State;
            timer2.Enabled = t2State;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e) // 打开文件
        {
            string filename = openFileDialog1.FileName;
            DoLoadMap(filename);
        }

        private void DoLoadMap(string filename)
        {
            if (LoadMap(filename))
            {
                Bitmap bmp = new Bitmap(PlayGround.MaxX + 10, PlayGround.MaxY + 10); // 新建画布
                Graphics g = Graphics.FromImage(bmp);  // 获取这块内存画布的Graphics引用
                g.Clear(this.BackColor);
                PlayGround.InitialPlayGround();
                InitialAnt();
                Pen pb = new Pen(_borderColor, 2); // 画边框
                Pen ph = new Pen(_homeColor, 2); // 画窝
                Pen pf = new Pen(_foodColor, 2); // 画食物
                g.DrawEllipse(pf, PlayGround.FoodLocation.X, PlayGround.FoodLocation.Y, 6, 6);
                g.DrawEllipse(ph, PlayGround.HomeLocation.X, PlayGround.HomeLocation.Y, 6, 6);
                g.DrawRectangle(pb, 1, 1, PlayGround.MaxX + 3, PlayGround.MaxY + 3);
                DrawStone(bmp);
                this.CreateGraphics().DrawImage(bmp, 0, 0); // 显示新建的画布
                bmp.Dispose();      // 释放内存
            }
        }

        private bool LoadMap(string filename)
        {
            try
            {
                Bitmap bmpst = new Bitmap(filename);
                if (bmpst.Width != PlayGround.MaxX || bmpst.Height != PlayGround.MaxY)
                {
                    PlayGround.ShowMessage = string.Format("地图格式错！图片宽{0}-高{1}，必须为宽{2}-高{3}，", bmpst.Width,bmpst.Height,PlayGround.MaxX,PlayGround.MaxY);
                    label2.Text = PlayGround.ShowMessage;
                    return false;
                }
                for (int i = 0; i < PlayGround.MaxX; i++)
                    for (int j = 0; j < PlayGround.MaxY; j++)
                    {
                        uint pArgb = (uint) bmpst.GetPixel(i, j).ToArgb();
                        //string sss = string.Format("{0:X}",pArgb);
                        if (pArgb==0x0 || pArgb == 0xFF000000) PlayGround.SetStone(i, j, false);
                        else PlayGround.SetStone(i,j,true);
                    }
                bmpst.Dispose();
            }
            catch (Exception ee)
            {
                PlayGround.ShowMessage = ee.Message;
                label2.Text = PlayGround.ShowMessage;
                return false;
            }
            string sfn = filename.IndexOf("\\")!=-1? filename.Substring(filename.LastIndexOf("\\")+1):filename;
            PlayGround.ShowMessage = string.Format("调入地图成功 - {0}", sfn);
            label2.Text = PlayGround.ShowMessage;
            return true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                textBox1.Text = "10";
            }
            int ttt = Convert.ToInt32(textBox1.Text);
            if (ttt <= 0) ttt = 10;
            timer1.Interval = ttt;
        }

        private void button2_Click(object sender, EventArgs e) // 信息释放率
        {
            if (string.IsNullOrEmpty(textBox2.Text))
            {
                textBox2.Text = Antc._smellDropRate.ToString();
            }
            float value;
            try
            {
                value = float.Parse(textBox2.Text);
            }
            catch
            {
                textBox2.Text = Antc._smellDropRate.ToString();
                value = Antc._smellDropRate;
            }
            for (int i = 0; i < MaxAnt; i++) Ants[i].SetSmellDropRate(value);
        }

        private void button3_Click(object sender, EventArgs e) // 出错概率
        {
            if (string.IsNullOrEmpty(textBox3.Text))
            {
                textBox3.Text = Antc._errorRate.ToString();
            }
            float value;
            try
            {
                value = float.Parse(textBox3.Text);
            }
            catch
            {
                textBox3.Text = Antc._errorRate.ToString();
                value = Antc._errorRate;
            }
            for (int i = 0; i < MaxAnt; i++) Ants[i].SetErrorRate(value);
        }

        private void button4_Click(object sender, EventArgs e) // 嗅觉范围
        {
            if (string.IsNullOrEmpty(textBox4.Text))
            {
                textBox4.Text = Antc._eyeShot.ToString();
            }
            int value;
            try
            {
                value = int.Parse(textBox4.Text);
            }
            catch
            {
                textBox4.Text = Antc._eyeShot.ToString();
                value = Antc._eyeShot;
            }
            for (int i = 0; i < MaxAnt; i++) Ants[i].SetEyeShot(value);
        }

        private void button5_Click(object sender, EventArgs e) // 最大信息素
        {
            if (string.IsNullOrEmpty(textBox5.Text))
            {
                textBox5.Text = Antc._maxSmell.ToString();
            }
            int value;
            try
            {
                value = int.Parse(textBox5.Text);
            }
            catch
            {
                textBox5.Text = Antc._maxSmell.ToString();
                value = Antc._maxSmell;
            }
            for (int i = 0; i < MaxAnt; i++) Ants[i].SetMaxSmell(value);
        }

        private void button6_Click(object sender, EventArgs e) // 路径记录最大值
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                textBox6.Text = Antc._maxTrace.ToString();
            }
            int value;
            try
            {
                value = int.Parse(textBox6.Text);
            }
            catch
            {
                textBox6.Text = Antc._maxTrace.ToString();
                value = Antc._maxTrace;
            }
            for (int i = 0; i < MaxAnt; i++) Ants[i].SetMaxTrace(value);
        }

        private void button8_Click(object sender, EventArgs e) // 信息素消逝速度
        {
            if (string.IsNullOrEmpty(textBox7.Text))
            {
                textBox7.Text = PlayGround._smellGoneRate.ToString();
            }
            float value;
            try
            {
                value = float.Parse(textBox7.Text);
            }
            catch
            {
                textBox7.Text = PlayGround._smellGoneRate.ToString();
                value = PlayGround._smellGoneRate;
            }
            PlayGround.SetSmellGoneRate(value);
        }

        private void button9_Click(object sender, EventArgs e) // 障碍物数量
        {
            if (string.IsNullOrEmpty(textBox8.Text))
            {
                textBox8.Text = PlayGround._maxStone.ToString();
            }
            int value;
            try
            {
                value = int.Parse(textBox8.Text);
            }
            catch
            {
                textBox8.Text = PlayGround._maxStone.ToString();
                value = PlayGround._maxStone;
            }
            PlayGround.SetMaxStone(value);
        }

        private void button10_Click(object sender, EventArgs e) // 蚂蚁数量
        {
            bool timerState = timer1.Enabled;
            timer1.Enabled = false;
            if (string.IsNullOrEmpty(textBox9.Text))
            {
                textBox9.Text = _maxAnt.ToString();
            }
            int value;
            try
            {
                value = int.Parse(textBox9.Text);
            }
            catch
            {
                textBox9.Text = _maxAnt.ToString();
                value = _maxAnt;
            }
            MaxAnt = value;
            timer1.Enabled = timerState;
            DoRestart();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) // 窝信息素
        {
            if (checkBox1.Checked)
            {
                DrawSmellCounter = 100;
                ClearSmellCounter = 0;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e) // 食物信息素
        {
            if (checkBox2.Checked)
            {
                DrawSmellCounter = 100;
                ClearSmellCounter = 0;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e) // 蚂蚁隐身
        {
            if (!checkBox3.Checked)
            {
                ClearAntCounter = 0;
            }
        }

        #region Member Fields
        private bool StoneInitialFlag = true;
        private int MaxAnt = _maxAnt;
        private Antc[] Ants = new Antc[_maxAnt];
        private Color _antColor = Color.Black;
        private Color _antfColor = Color.Purple;
        private Color _homeColor = Color.Green;
        private Color _foodColor = Color.Blue;
        private Color _borderColor = Color.Brown;
        private Color _stoneColor = Color.Brown;
        private Color _hSmellColor = Color.LightGreen;
        private Color _fSmellColor = Color.LightBlue;
        private int ShowMessageCounter = 0;
        private int DrawSmellCounter = 0;
        private int DealSmellCounter = 0;
        private int ClearAntCounter = 0;
        private int ClearSmellCounter = 0;
        private const int _maxAnt = 50;
        #endregion
    }
}