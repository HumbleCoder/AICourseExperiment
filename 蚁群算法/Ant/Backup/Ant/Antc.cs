using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ant
{
    class Antc
    {
        public Antc(int seed)
        {
            NowLocation.X = PlayGround.HomeLocation.X;
            NowLocation.Y = PlayGround.HomeLocation.Y;
            //LastLocation.X = NowLocation.X;
            //LastLocation.Y = NowLocation.Y;
            ran = new Random(seed);
            NowDirection = (AntDirection) ran.Next(4) + 1; // 4：只使用4个方向，8：使用8个方向
            HomeSmellNum = MaxSmell; // 蚂蚁身上携带的Home信息素的数量
            FoodSmellNum = 0;
            FoodNum = 0;
            ClearTrace();
        }

        private void ClearTrace()
        {
            for (int i = 0; i < MaxTrace; i++)
            {
                Trace[i].X = 0;
                Trace[i].Y = 0;
            }
        }

        private void DropHomeSmell()
        {
            if (HomeSmellNum <= 0) return;
            int groundsmell = PlayGround.HomeSmell[NowLocation.X, NowLocation.Y];
            int smelldrop = (int) (HomeSmellNum * SmellDropRate);
            if (smelldrop > groundsmell) PlayGround.HomeSmell[NowLocation.X, NowLocation.Y] = smelldrop;
            HomeSmellNum -= smelldrop;
            if (HomeSmellNum < 0) HomeSmellNum = 0;
        }

        private void DropFoodSmell()
        {
            if (FoodSmellNum <= 0) return;
            int groundsmell = PlayGround.FoodSmell[NowLocation.X, NowLocation.Y];
            int smelldrop = (int)(FoodSmellNum * SmellDropRate);
            if (smelldrop > groundsmell) PlayGround.FoodSmell[NowLocation.X, NowLocation.Y] = smelldrop;
            FoodSmellNum -= smelldrop;
            if (FoodSmellNum < 0) FoodSmellNum = 0;
        }

        public void AntOneStep() 
        /* 1、决定调用函数决定蚂蚁的下一步方向
         * 2、蚂蚁移动
         * 3、如果找到食物，或者回到窝，进行响应的处理
         * 4、释放信息素
         * 5、记录走过的路径？
         */
        {
            AntDirection ddir;
            int tttx, ttty;

            tttx = NowLocation.X;
            ttty = NowLocation.Y;
            ddir = NowDirection;

            ddir = NextDirection(tttx, ttty, ddir);

            switch (ddir)
            {
                case AntDirection.Up: ttty--;
                    break;
                case AntDirection.Down: ttty++;
                    break;
                case AntDirection.Left: tttx--;
                    break;
                case AntDirection.Right: tttx++;
                    break;
                default: break;
            } /* of switch dir */
            NowLocation.X = tttx;
            NowLocation.Y = ttty;
            NowDirection = ddir;

            // 窝处理
            if (tttx == PlayGround.HomeLocation.X && ttty == PlayGround.HomeLocation.Y)
            {
                HomeSmellNum = MaxSmell;
                ClearTrace();
                if (FoodNum > 0)
                {
                    PlayGround.HomeNum += FoodNum;
                    FoodNum = 0;
                    FoodSmellNum = 0;
                    NowDirection = (AntDirection)ran.Next(4) + 1; // 4：只使用4个方向，8：使用8个方向
                    PlayGround.ShowMessage2 = string.Format("食物剩余{0}，窝里的食物{1}", PlayGround.FoodNum, PlayGround.HomeNum);
                }
            }
            // 找到食物的处理
            else if (tttx == PlayGround.FoodLocation.X && ttty == PlayGround.FoodLocation.Y)
            {
                FoodSmellNum = MaxSmell;
                ClearTrace();
                if (FoodNum <= 0)
                {
                    FoodNum += 1;
                    PlayGround.FoodNum -= 1;
                    HomeSmellNum = 0;
                    NowDirection = TurnBack(NowDirection);
                    PlayGround.ShowMessage2 = string.Format("食物剩余{0}，窝里的食物{1}", PlayGround.FoodNum, PlayGround.HomeNum);
                }
            }
            else
            {
                // 留下信息素
                DropHomeSmell();
                DropFoodSmell();
                // 记录Trace
                Trace[TracePtr].X = tttx;
                Trace[TracePtr].Y = ttty;
                TracePtr++;
                if (TracePtr >= MaxTrace) TracePtr = 0;
            }
        }

        private AntDirection NextDirection(int xxx,int yyy,AntDirection ddir)
        {
            int CanGoState = WhereCanGo(xxx, yyy, ddir);
            bool cangof, cangol, cangor; // 分别表示前面、左边、右边能走
            if (CanGoState == 0 || CanGoState == 2 || CanGoState == 3 || CanGoState == 6) cangof = true;
            else cangof = false;
            if (CanGoState == 0 || CanGoState == 1 || CanGoState == 3 || CanGoState == 5) cangol = true;
            else cangol = false;
            if (CanGoState == 0 || CanGoState == 1 || CanGoState == 2 || CanGoState == 4) cangor = true;
            else cangor = false;

            int type; // 表示现在要寻找窝还是食物
            if(FoodNum>0) type = _smellTypeHome;
            else type = _smellTypeFood;

            int msf, msl, msr; // 分别记录前面、左边、右边的最大信息素数值
            int maxms; /* 1 表示前面的信息素最大
                        * 2 表示左边的信息素最大
                        * 3 表示右边的信息素最大
                        */
            msf = GetMaxSmell(type, xxx, yyy, ddir);
            msl = GetMaxSmell(type, xxx, yyy, TurnLeft(ddir));
            msr = GetMaxSmell(type, xxx, yyy, TurnRight(ddir));
            maxms = MaxLocation(msf, msl, msr);
  
            AntDirection testdir = AntDirection.Null;
            switch (maxms)
            {
                case 0: /* all is 0, keep testdir = NULL, random select dir */
                    break;
                case 1: 
                    if (cangof)
                        testdir = ddir;
                    else
                        if (msl > msr) if (cangol) testdir = TurnLeft(ddir);
                            else if (cangor) testdir = TurnRight(ddir);
                    break;
                case 2: 
                    if (cangol)
                        testdir = TurnLeft(ddir);
                    else
                        if (msf > msr) if (cangof) testdir = ddir;
                            else if (cangor) testdir = TurnRight(ddir);
                    break;
                case 3: 
                    if (cangor)
                        testdir = TurnRight(ddir);
                    else
                        if (msf > msl) if (cangof) testdir = ddir;
                            else if (cangol) testdir = TurnLeft(ddir);
                    break;
                default: break;
            } /* of maxms */

            if (testdir == AntDirection.Null || ran.Next(10000) < (int) (ErrorRate * 10000.0F)) return RandomDirection(xxx, yyy, ddir);
            else return testdir;
        }

        private AntDirection RandomDirection(int xxx, int yyy, AntDirection ddir)
        {

            int randnum = ran.Next(1000);
            int CanGoState = WhereCanGo(xxx, yyy, ddir);
            AntDirection testdir;
            switch (CanGoState)
            {
                case 0: if (randnum < 950) testdir = ddir;
                        else if (randnum >= 950 && randnum < 975) testdir = TurnLeft(ddir);
                        else testdir = TurnRight(ddir);
                        break;
                case 1: if (randnum < 500) testdir = TurnLeft(ddir);
                        else testdir = TurnRight(ddir);
                        break;
                case 2: if (randnum < 950) testdir = ddir;
                        else testdir = TurnRight(ddir);
                        break;
                case 3: if (randnum < 950) testdir = ddir;
                        else testdir = TurnLeft(ddir);
                        break;
                case 4: testdir = TurnRight(ddir);
                        break;
                case 5: testdir = TurnLeft(ddir);
                        break;
                case 6: testdir = ddir;
                        break;
                case 7: testdir = TurnBack(ddir);
                        break;
                default: testdir = TurnBack(ddir);
                        break;
            } /* of can go state */
            return testdir;
        }

        int GetMaxSmell(int type, int xxx, int yyy, AntDirection ddir)
        {
            int i, j;
            int ms; /* MAX smell */

            ms = 0;
            switch (ddir)
            {
                case AntDirection.Up: 
                    for (i = xxx - EyeShot; i <= xxx + EyeShot; i++)
                        for (j = yyy - EyeShot; j < yyy; j++)
                        {
                            if (!JudgeCanGo(i, j)) continue;
                            if ((i == PlayGround.FoodLocation.X && j == PlayGround.FoodLocation.Y && type == _smellTypeFood) ||
                                (i == PlayGround.HomeLocation.X && j == PlayGround.HomeLocation.Y && type == _smellTypeHome))
                            {
                                ms = MaxSmell;
                                break;
                            }
                            if (IsTrace(i, j)) continue;
                            if (type == _smellTypeFood) if (PlayGround.FoodSmell[i, j] > ms) ms = PlayGround.FoodSmell[i, j];
                            if (type == _smellTypeHome) if (PlayGround.HomeSmell[i, j] > ms) ms = PlayGround.HomeSmell[i, j];
                        }
                    break;
                case AntDirection.Down: 
                    for (i = xxx - EyeShot; i <= xxx + EyeShot; i++)
                        for (j = yyy + 1; j <= yyy + EyeShot; j++)
                        {
                            if (!JudgeCanGo(i, j)) continue;
                            if ((i == PlayGround.FoodLocation.X && j == PlayGround.FoodLocation.Y && type == _smellTypeFood) ||
                                (i == PlayGround.HomeLocation.X && j == PlayGround.HomeLocation.Y && type == _smellTypeHome))
                            {
                                ms = MaxSmell;
                                break;
                            }
                            if (IsTrace(i, j)) continue;
                            if (type == _smellTypeFood) if (PlayGround.FoodSmell[i, j] > ms) ms = PlayGround.FoodSmell[i, j];
                            if (type == _smellTypeHome) if (PlayGround.HomeSmell[i, j] > ms) ms = PlayGround.HomeSmell[i, j];
                        }
                    break;
                case AntDirection.Left: 
                    for (i = xxx - EyeShot; i < xxx; i++)
                        for (j = yyy - EyeShot; j <= yyy + EyeShot; j++)
                        {
                            if (!JudgeCanGo(i, j)) continue;
                            if ((i == PlayGround.FoodLocation.X && j == PlayGround.FoodLocation.Y && type == _smellTypeFood) ||
                                (i == PlayGround.HomeLocation.X && j == PlayGround.HomeLocation.Y && type == _smellTypeHome))
                            {
                                ms = MaxSmell;
                                break;
                            }
                            if (IsTrace(i, j)) continue;
                            if (type == _smellTypeFood) if (PlayGround.FoodSmell[i, j] > ms) ms = PlayGround.FoodSmell[i, j];
                            if (type == _smellTypeHome) if (PlayGround.HomeSmell[i, j] > ms) ms = PlayGround.HomeSmell[i, j];
                        }
                    break;
                case AntDirection.Right: 
                    for (i = xxx + 1; i <= xxx + EyeShot; i++)
                        for (j = yyy - EyeShot; j <= yyy + EyeShot; j++)
                        {
                            if (!JudgeCanGo(i, j)) continue;
                            if ((i == PlayGround.FoodLocation.X && j == PlayGround.FoodLocation.Y && type == _smellTypeFood) ||
                                (i == PlayGround.HomeLocation.X && j == PlayGround.HomeLocation.Y && type == _smellTypeHome))
                            {
                                ms = MaxSmell;
                                break;
                            }
                            if (IsTrace(i, j)) continue;
                            if (type == _smellTypeFood) if (PlayGround.FoodSmell[i, j] > ms) ms = PlayGround.FoodSmell[i, j];
                            if (type == _smellTypeHome) if (PlayGround.HomeSmell[i, j] > ms) ms = PlayGround.HomeSmell[i, j];
                        }
                    break;
                default: break;
            }
            return ms;
        }

        bool IsTrace(int xxx, int yyy)
        {
            for (int i = 0; i < MaxTrace; i++)
                if (Trace[i].X == xxx && Trace[i].Y == yyy) return true;
            return false;
        }

        /* 返回：
         * 1 - 第1个数 is MAX
           2 - 第2个数 is MAX
           3 - 第3个数 is MAX 
           0 - all 3 number is 0
         */
        int MaxLocation(int num1, int num2, int num3)
        {
            int maxnum;

            if (num1 == 0 && num2 == 0 && num3 == 0) return 0;

            maxnum = num1;
            if (num2 > maxnum) maxnum = num2;
            if (num3 > maxnum) maxnum = num3;

            if (maxnum == num1) return 1;
            if (maxnum == num2) return 2;
            if (maxnum == num3) return 3;
            return 0;
        }

// ==========================  一些子程序 ================
        private int WhereCanGo(int xxx, int yyy, AntDirection ddir)
        /* input: xxx,yyy - location of ant
                  ddir - now dir
           output: 0 - forward and left and right can go
                   1 - forward can not go
                   2 - left can not go
                   3 - right can not go
                   4 - forward and left can not go
                   5 - forward and right can not go
                   6 - left and right can not go
                   7 - forward and left and right all can not go
        */
        {
            int tx, ty;
            AntDirection tdir;
            bool okf, okl, okr;

            /* forward can go ? */
            tdir = ddir;
            tx = xxx;
            ty = yyy;
            switch (tdir)
            {
                case AntDirection.Up: ty--;
                    break;
                case AntDirection.Down: ty++;
                    break;
                case AntDirection.Left: tx--;
                    break;
                case AntDirection.Right: tx++;
                    break;
                default: break;
            } /* of switch dir */
            if (JudgeCanGo(tx, ty)) okf = true;
            else okf = false;

            /* turn left can go ? */
            tdir = TurnLeft(ddir);
            tx = xxx;
            ty = yyy;
            switch (tdir)
            {
                case AntDirection.Up: ty--;
                    break;
                case AntDirection.Down: ty++;
                    break;
                case AntDirection.Left: tx--;
                    break;
                case AntDirection.Right: tx++;
                    break;
                default: break;
            } /* of switch dir */
            if (JudgeCanGo(tx, ty)) okl = true;
            else okl = false;

            /* turn right can go ? */
            tdir = TurnRight(ddir);
            tx = xxx;
            ty = yyy;
            switch (tdir)
            {
                case AntDirection.Up: ty--;
                    break;
                case AntDirection.Down: ty++;
                    break;
                case AntDirection.Left: tx--;
                    break;
                case AntDirection.Right: tx++;
                    break;
                default: break;
            } /* of switch dir */
            if (JudgeCanGo(tx, ty)) okr = true;
            else okr = false;

            if (okf && okl && okr) return (0);
            if (!okf && okl && okr) return (1);
            if (okf && !okl && okr) return (2);
            if (okf && okl && !okr) return (3);
            if (!okf && !okl && okr) return (4);
            if (!okf && okl && !okr) return (5);
            if (okf && !okl && !okr) return (6);
            if (!okf && !okl && !okr) return (7);
            return (7);
        }

        private bool JudgeCanGo(int xxx,int yyy)
        {
            if (xxx < 3 || xxx >= PlayGround.MaxX) return false;
            if (yyy < 3 || yyy >= PlayGround.MaxY) return false;
            for (int iii = xxx - AntSize; iii <= xxx + AntSize; iii++)
            {
                if (iii < 0 || iii >= PlayGround.MaxX) continue;
                for (int jjj = yyy - AntSize; jjj <= yyy + AntSize; jjj++)
                {
                    if (jjj < 0 || jjj >= PlayGround.MaxY) continue;
                    if (PlayGround.IsStone(iii, jjj)) return false;
                }
            }
            return (true);
        }

        private AntDirection TurnLeft(AntDirection ddir)
        {
            switch (ddir)
            {
                case AntDirection.Up: return AntDirection.Left;
                case AntDirection.Down: return AntDirection.Right;
                case AntDirection.Left: return AntDirection.Down;
                case AntDirection.Right: return AntDirection.Up;
                default: return AntDirection.Null;
            } /* of switch dir */
        }

        private AntDirection TurnRight(AntDirection ddir)
        {
            switch (ddir)
            {
                case AntDirection.Up: return AntDirection.Right;
                case AntDirection.Down: return AntDirection.Left;
                case AntDirection.Left: return AntDirection.Up;
                case AntDirection.Right: return AntDirection.Down;
                default: return AntDirection.Null;
            } /* of switch dir */
        }

        private AntDirection TurnBack(AntDirection ddir)
        {
            switch (ddir)
            {
                case AntDirection.Up: return AntDirection.Down;
                case AntDirection.Down: return AntDirection.Up;
                case AntDirection.Left: return AntDirection.Right;
                case AntDirection.Right: return AntDirection.Left;
                default: return AntDirection.Null;
            } /* of switch dir */
        }

// ======================  一些接口函数  =====================
        public int GetX()
        {
            return NowLocation.X;
        }

        public int GetY()
        {
            return NowLocation.Y;
        }

        public bool HaveFood()
        {
            if (FoodNum > 0) return true;
            else return false;
        }

        // 以下这几个设置可以实时更改
        public void SetSmellDropRate(float value)
        {
            if (value >= 1|| value <= 0) return;
            SmellDropRate = value;
            PlayGround.ShowMessage = string.Format("信息素释放率更改为{0}",value);
        }

        public void SetErrorRate(float value)
        {
            if (value >= 1 || value <= 0) return;
            ErrorRate = value;
            PlayGround.ShowMessage = string.Format("蚂蚁出错概率更改为{0}", value);
        }

        public void SetEyeShot(int value)
        {
            if (value >= PlayGround.MaxX || value>=PlayGround.MaxY || value <= 0) return;
            EyeShot = value;
            PlayGround.ShowMessage = string.Format("蚂蚁嗅觉范围更改为{0}", value);
        }

        // 以下几个设置更改后，需要重新初始化
        public void SetMaxSmell(int value)
        {
            if (value <= 0) return;
            MaxSmell = value;
            HomeSmellNum = 0;
            FoodSmellNum = 0;
            PlayGround.ShowMessage = string.Format("最大信息素更改为{0}", value);
        }

        public void SetMaxTrace(int value)
        {
            if (value <= 0) return;
            MaxTrace = value;
            TracePtr = 0;
            Trace = new Point[MaxTrace];
            PlayGround.ShowMessage = string.Format("路径记录最大值更改为{0}", value);
        }

// ========================  数据定义 ============================
        public enum AntDirection:int
        {
            Null,Up,Down,Left,Right,UpLeft,UpRight,DownLeft,DownRIght
        }

        #region Member Fields
        Random ran;
        // private Point LastLocation; // 上次的位置
        private Point NowLocation;  // 现在的位置
        private AntDirection NowDirection = AntDirection.Null;
        private int HomeSmellNum = _maxSmell; // 蚂蚁身上携带的Home信息素的数量
        private int FoodSmellNum = 0;
        private int FoodNum = 0;
        private int MaxSmell = _maxSmell;
        private float SmellDropRate = _smellDropRate;
        private float ErrorRate = _errorRate;
        private int EyeShot = _eyeShot;
        public const int AntSize = 2;
        private const int _smellTypeFood = 0;
        private const int _smellTypeHome = 1;
        private int MaxTrace = 50;
        private Point[] Trace = new Point[_maxTrace];
        private int TracePtr = 0;
        public const int _maxSmell = 100000;  // 原来的设定是5000
        public const float _smellDropRate = 0.005F;  // 原来设定是0.05
        public const float _errorRate = 0.2F;  // 原来是0.02
        public const int _eyeShot = 2;  // 原来是3
        public const int _maxTrace = 50;  // 原来是50
        #endregion
    }
}
