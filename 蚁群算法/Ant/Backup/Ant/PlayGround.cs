using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ant
{
    public static class PlayGround
    {
        public static void InitialPlayGround()
        {
            HomeNum = 0;
            FoodNum = 10000;
            HomeSmell = new int[MaxX, MaxY];
            FoodSmell = new int[MaxX, MaxY];
            ShowMessage = "";
            ShowMessage2 = string.Format("食物剩余{0}，窝里的食物{1}", FoodNum, HomeNum);
        }

        public static void InitialFood()
        {

        }

        public static void InitialStone()
        {
            int i,j;
            int bn;
            
            for(i=0;i<MaxX;i++)
                for(j=0;j<MaxY;j++)
                    Stone[i,j] = false;

            bn = 1+ MaxStone/2 + ran.Next(MaxStone/2);
            for(i=0;i<=bn;i++) CreatStone();
        }

        private static void CreatStone()
        {
            int x1,y1,x2,y2;
            int dx,dy;
            int i,j;

            x1 = ran.Next(MaxX) + 1;
            y1 = ran.Next(MaxY) + 1;

            dx = ran.Next(MaxX / 10) + 1;
            dy = ran.Next(MaxY / 10) + 1;
            
            x2 = x1+dx;
            y2 = y1+dy;
            
            if(x2>=MaxX) x2 = MaxX-1;
            if(y2>=MaxY) y2 = MaxY-1;

            if (HomeLocation.X >= x1-8 && HomeLocation.X <= x2+8 && HomeLocation.Y >= y1-8 && HomeLocation.Y <= y2+8) return;
            if (FoodLocation.X >= x1 - 8 && FoodLocation.X <= x2 + 8 && FoodLocation.Y >= y1 - 8 && FoodLocation.Y <= y2 + 8) return;
            
            for(i=x1;i<=x2;i++)
                for(j=y1;j<=y2;j++)
                {
                    Stone[i,j] = true;
                }
        }

        public static void DealGround()
        {
            int hsm, fsm;
            for (int xxx = 3; xxx < MaxX; xxx++)
                for (int yyy = 3; yyy < MaxY; yyy++)
                {
                    hsm = HomeSmell[xxx, yyy];
                    fsm = FoodSmell[xxx, yyy];
                    if (hsm <= 0 && fsm<=0) continue;
                    if (hsm > 0)
                    {
                        HomeSmell[xxx, yyy] -= (int)(1 + (float)hsm * SmellGoneRate);
                        if (HomeSmell[xxx, yyy] <= 0) HomeSmell[xxx, yyy] = 0;
                    }
                    if (fsm > 0)
                    {
                        FoodSmell[xxx, yyy] -= (int)(1 + (float)fsm * SmellGoneRate);
                        if (FoodSmell[xxx, yyy] <= 0) FoodSmell[xxx, yyy] = 0;
                    }
                }
        }

        public static bool IsStone(int xxx, int yyy)
        {
            if (Stone[xxx, yyy]) return true;
            else return false;
        }

        public static void SetStone(int xxx, int yyy, bool stone)
        {
            if (stone) Stone[xxx, yyy] = true;
            else Stone[xxx, yyy] = false;
        }

// ================== 子程序 =====================
        public static void SetSmellGoneRate(float value)
        {
            if (value >= 1 || value <= 0) return;
            SmellGoneRate = value;
            PlayGround.ShowMessage = string.Format("信息素消失率更改为{0}", value);
        }

        public static void SetMaxStone(int value)
        {
            if (value <= 0) return;
            MaxStone = value;
            PlayGround.ShowMessage = string.Format("障碍物数量更改为{0}", value);
        }

// ================  数据定义  ==============================

        #region Member Fields
        private static Random ran = new Random();
        public const int MaxX = 200;  // MAX is 330, 280
        public const int MaxY = 200;
        public static Point HomeLocation = new Point(10,10);
        public static Point FoodLocation = new Point(MaxX-10, MaxY-10);
        public static int HomeNum = 0;
        public static int FoodNum = 10000;
        public static int[,] HomeSmell = new int[MaxX, MaxY];
        public static int[,] FoodSmell = new int[MaxX, MaxY];
        private static bool[,] Stone = new bool[MaxX, MaxY];
        private static float SmellGoneRate = _smellGoneRate;
        private static int MaxStone = _maxStone;
        public static string ShowMessage = ""; // 优先显示ShowMessage，而且只显示一次
        public static string ShowMessage2 = ""; // 如果ShowMessage显示过，则显示ShowMessage2
        public const float _smellGoneRate = 0.05F; // 原来设定是0.05
        public const int _maxStone = 150;  // 原来是100
        #endregion
    }
}
